DROP TABLE IF EXISTS item;

CREATE TABLE client (
  client_Id INT AUTO_INCREMENT  PRIMARY KEY,
  first_Name VARCHAR(250) NOT NULL,
  last_Name VARCHAR(250) NOT NULL,
  birthday DATETIME NOT NULL,
  gender VARCHAR(25) NOT NULL,
  client_Cellphone VARCHAR(50) NOT NULL,
  client_Homephone VARCHAR(50) NOT NULL,
  address_Home VARCHAR(250) NOT NULL,
  profession VARCHAR(50) NOT NULL,
  client_Income DOUBLE NOT NULL
);


