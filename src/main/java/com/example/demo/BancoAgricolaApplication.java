package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class BancoAgricolaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoAgricolaApplication.class, args);
	}

	@Bean
	   public Docket productApi() {
		String groupName = "Swagger";
	      return new Docket(DocumentationType.SWAGGER_2).select()
	         .apis(RequestHandlerSelectors.basePackage("com.example.demo")).build()
          .groupName(groupName)
          .apiInfo(apiInfo());
	   }
             

    // Describe the apis
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Prueba")
                .description("Unicomer Client")
                .version("1.0.0")
                .license("Licencia")
                .build();
    }
}
