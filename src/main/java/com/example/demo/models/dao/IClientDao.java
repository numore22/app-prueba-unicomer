package com.example.demo.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.models.entities.Client;

public interface IClientDao extends CrudRepository<Client, Long>{

}
